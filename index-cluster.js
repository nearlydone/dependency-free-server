const cluster = require('cluster')
const os = require('os')

const server = require('./01-server')
const worker = require('./02-workers')
const cli = require('./03-cli')

const app = {
	init: cb => {
		if (cluster.isMaster) {
			worker.init()
			setTimeout(() => {
				if (require.main === module) {
					cli.init()
				}
				if (cb) {
					cb()
				}
			}, 100)
			os.cpus().forEach(() => {
				cluster.fork()
			})
		} else {
			server.init()
		}
	}
}

if (require.main === module) {
	app.init()
}

module.exports = app