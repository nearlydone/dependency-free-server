
const client = {
	buttons: () => {
		document.getElementById('logout').addEventListener('click', e => {
			e.preventDefault()
			client.logout()
		})
	},
	checksListData: () => {
		client.get.sessionToken()
		const query = {
			phoneNumber: client.config.sessionToken.phoneNumber
		}
		client.talk.server(undefined, 'api/users', 'GET', query, undefined, (statusCode, response) => {
			if (!response) {
				return false
			}
			if (statusCode === 200) {
				if (response.checks && response.checks.length) {
					document.getElementById('nochecks').style.display = 'none';
					response.checks.forEach(checkId => {
						const idQuery = {
							id: checkId
						};
						client.talk.server(undefined, 'api/check', 'GET', idQuery, undefined, (statusCode, response) => {
							if (statusCode === 200) {
								const table = document.getElementById('checks');
								const tr = table.insertRow(-1);
								tr.classList.add('checkRow');
								const td0 = tr.insertCell(0);
								td0.innerHTML = response.method.toUpperCase();
								const td1 = tr.insertCell(1);
								td1.innerHTML = response.protocol+'://';
								const td2 = tr.insertCell(2);
								td2.innerHTML = response.url;
								const td3 = tr.insertCell(3);
								td3.innerHTML = response.state;
								const td4 = tr.insertCell(4);
								td4.innerHTML = `<a href="/checks/edit?id=${response.id}">View / Edit / Delete</a>`;
							} else {
								console.log('Error trying to load check ID: ',checkId);
							}
						});
					});
					if (response.checks.length < 5) {
						document.getElementById('cta').style.display = 'block';
					}
				} else {
					document.getElementById('nochecks').style.display = 'table-row';
					document.getElementById('cta').style.display = 'block';
				}
			} else {
				client.logout();
			}
		});
	},
	config: {
		sessionToken: false
	},
	data: () => {
		const className = document.querySelector('body').classList[0]
		if (className === 'checksList') {
			client.checksListData()
		}
	},
	forms: () => {
		if (document.querySelector('form') === null) {
			return false
		}
		document.querySelector('form').addEventListener('submit', e => {
			e.preventDefault()
			document.querySelector(`#${e.target.id} .error`).style.display = 'none'
			const payload = {}
			const inputs = e.target.elements
			Object.keys(inputs).forEach(ref => {
				let value
				if (inputs[ref].type === 'checkbox') {
					value = inputs[ref].checked
				} else {
					value = inputs[ref].value
				}
				payload[inputs[ref].name] = value
			})
			// TODO:: Read values from checkboxes and populate array
			payload.successCodes = [200, 201]
			client.talk.server(null, e.target.action, e.target.attributes.method.value, null, payload, (status, data) => {
				if (status === 200) {
					client.process.formResponse(e.target.id, payload, data)
				} else {
				document.querySelector(`#${e.target.id} .error`).style.display = 'block'
				document.querySelector(`#${e.target.id} .error`).innerHTML = data.error
				}
			})
		})
	},
	get: {
		sessionToken: () => {
			const storedToken = localStorage.getItem('token')
			if (storedToken) {
				try {
					client.config.sessionToken = JSON.parse(storedToken)
					client.set.loggedInStatus(true)
				} catch(e) {
					client.config.sessionToken = false
					client.set.loggedInStatus(false)
				}
			}
		}
	},
	init: () => {
		client.buttons()
		client.forms()
		client.data()
		setInterval(client.renewToken, 60 * 1000)
	},
	logout: () => {
		const query = {
			id: client.config.sessionToken
		}
		client.talk.server(null, 'api/tokens', 'delete', query, null, () => {
			client.set.sessionToken(false)
		})
		window.location = 'session/destroy'
	},
	process: {
		formResponse: (id, original, response) => {
			if (id === 'accountCreate') {
				const sessionPayload = {
					phoneNumber: original.phoneNumber,
					password: original.password
				}
				client.talk.server(null, 'api/tokens', 'post', null, sessionPayload, (status, token) => {
					if (status === 200) {
						client.set.sessionToken(token)
						window.location = '/checks/list'
					}
				})
			}
			if (id === 'accountDelete') {
				window.location = '/account/deleted'
			}
			if (id === 'checksCreate') {
				window.location = '/checks/list'
			}
			if (id === 'sessionCreate') {
				client.set.sessionToken(response)
				window.location = '/checks/list'
			}
		}
	},
	renewToken: cb => err => {
		if (!err) {
			console.log(`Token renewed at ${Date.now()}`)
		}
	},
	set: {
		loggedInStatus: (status) => {
			const body = document.querySelector('body')
			const loggedInClass = 'loggedin'
			if (status) {
				body.classList.add(loggedInClass)
			} else {
				body.classList.remove(loggedInClass)
			}
		},
		sessionToken: token => {
			localStorage.setItem('token', typeof token === 'string' ? token : JSON.stringify(token || {}))
		}
	},
	talk: {
		server: (headers, path, method, qs, payload, cb) => {
			cb = typeof(cb) === 'function' ? cb : false

			let count = 0
			for (let key in qs) {
				path += count ? '&' : '?'
				path += `${key}=${qs[key]}`
				count++
			}

			const xhr = new XMLHttpRequest()
			xhr.open(method, path, true)
			xhr.setRequestHeader('Content-Type', 'application/json')

			for (let key in headers) {
				if (headers.hasOwnProperty(key)) {
					xhr.setRequestHeader(key, headers[key])
				}
			}
			client.get.sessionToken()
			if (client.config.sessionToken) {
				xhr.setRequestHeader('token', client.config.sessionToken.id)
			}

			xhr.onreadystatechange = () => {
				if (xhr.readyState === XMLHttpRequest.DONE) {
					const code = xhr.status
					const response = xhr.responseText

					if (cb) {
						try {
							const parsed = JSON.parse(response)
							cb(code, parsed)
						} catch(e) {
							cb(code, false)
						}
					}
				}
			}

			xhr.send(JSON.stringify(payload))
		}
	}
}

window.onload = client.init
