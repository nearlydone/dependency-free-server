const readline = require('readline')
const util = require('util')
const debug = util.debug('cli')
const events = require('events')

const handlers = require('./lib/events')

class Events extends events{}
const e = new Events()

const cli = {
	commands: [
		'man',
		'help',
		'exit',
		'stats',
		'list users',
		'list checks',
		'list logs',
		'info user',
		'info check',
		'info log'
	],
	init: () => {
		console.log('\x1b[34m%s\x1b[0m', `CLI up and running\nCLI Commands:`)
		cli.commands.forEach((input, i) => {
			console.log('\x1b[34m%s\x1b[0m', `${++i}) ${input}`)
		})

		const interface = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
			prompt: '>> '
		})

		interface.prompt()

		interface.on('line', str => {
			cli.input(str)
			interface.prompt()
		})

		interface.on('close', handlers.exit)

		e.on('man', handlers.help)
		e.on('help', handlers.help)
		e.on('exit', handlers.exit)
		e.on('stats', handlers.stats)
		e.on('list users', handlers.list.users)
		e.on('list checks', handlers.list.checks)
		e.on('list logs', handlers.list.logs)
		e.on('info user', handlers.info.user)
		e.on('info check', handlers.info.check)
		e.on('info log', handlers.info.log)
	},
	input: str => {
		str = str.trim().length > 0 ? str : false

		if (str) {
			let matched = false
			let count = 0
			cli.commands.some(input => {
				if (str.toLowerCase().indexOf(input) > -1) {
					matched = true
					e.emit(input, str)
					return true
				}
			})

			if (!matched) {
				console.log('Bad command. Try again.')
			}
		}
	}
}

module.exports = cli
