const http = require('http')
const https = require('https')
const url = require('url')
const debug = require('util').debuglog('server')
const SD = require('string_decoder').StringDecoder
const config = require('./config')
const handlers = require('./lib/handlers')
const utils = require('./lib/utils')

const router = {
	'': handlers.index,
	'/': handlers.index,
	'account/create': handlers.account.create,
	'account/edit': handlers.account.edit,
	'account/deleted': handlers.account.deleted,
	'login': handlers.login,
	// 'loggedout': handlers.loggedout,
	'session/create': handlers.session.create,
	'session/destroy': handlers.session.delete,
	'checks/list': handlers.checks.list,
	'checks/create': handlers.checks.create,
	// 'checks/edit': handlers.checks.edit,
	'api/check': handlers.check,
	'api/users': handlers.users,
	'api/tokens': handlers.tokens,
	'health': handlers.healthCheck,
	'notFound': handlers.notFound,
	'favicon.ico': handlers.favicon,
	'public': handlers.public
}

const processRequest = (req, res) => {
	const headers = req.headers
	const method = req.method.toLowerCase()
	const parsedURL = url.parse(req.url, true)
	const path = parsedURL.pathname.replace(/^\/+|\/+$/g, '')
	const querys = parsedURL.query
	const decoder = new SD('utf-8')

	let buffer = ''
	req
		.on('data', data => {
			buffer += decoder.write(data)
			debug('part buffer', buffer)
		})
		.on('end', () => {
			buffer += decoder.end()

			const data = {
				headers,
				method,
				path,
				payload: buffer,
				querys
			}

			const cb = (status, payload, contentType) => {
				status = typeof(status) === 'number' ? status : 200
				contentType = contentType || 'json'

				if (contentType === 'json') {
					contentType = 'application/json'
					payload = typeof payload === 'string' ? payload : JSON.stringify(payload || {})
				}
				if (contentType === 'html') {
					contentType = 'text/html'
					payload = payload || ''
				}
				if (contentType === 'favicon') {
					contentType = 'image/x-icon'
					payload = payload || ''
				}
				if (contentType === 'css') {
					contentType = 'text/css'
					payload = payload || ''
				}
				if (contentType === 'png') {
					contentType = 'image/png'
					payload = payload || ''
				}
				if (contentType === 'jpg') {
					contentType = 'image/jpeg'
					payload = payload || ''
				}
				if (contentType === 'plain') {
					contentType = 'text/plain'
					payload = payload || ''
				}

				res.setHeader('Content-Type', contentType || 'application/json')
				res.writeHead(status)
				res.end(payload)
			}

			if (path.indexOf('public/') > -1) {
				router.public(data, cb)
			} else if (typeof(router[path]) !== 'undefined') {
				router[path](data, cb)
			} else {
				router.notFound(data, cb)
			}
		})

}

const hserver = http.createServer(processRequest)
const sserver = https.createServer({
	cert: config.cert,
	key: config.key
}, processRequest)

module.exports = {
	init: () => {
		hserver.listen(config.openport, () => {
			console.log('\x1b[36m%s\x1b[0m', `listening on ${config.openport} for ${config.envName}`)
		})

		sserver.listen(config.secureport, () => {
			console.log('\x1b[35m%s\x1b[0m', `listening on ${config.secureport} for ${config.envName}`)
		})
	}
}