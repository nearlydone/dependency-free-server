const server = require('./01-server')
const worker = require('./02-workers')
const cli = require('./03-cli')

const app = {
	init: cb => {
		server.init()
		worker.init()
		setTimeout(() => {
			if (require.main === module) {
				cli.init()
			}
			if (cb) {
				cb()
			}
		}, 100)
	}
}

if (require.main === module) {
	app.init()
}

module.exports = app