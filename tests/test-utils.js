const http = require('http')
const config = require('./../config')

const utils = {
	getRequest: (path, headers, cb) => {
		const params = {
			headers: {
				...headers,
				'Content-Type': 'application/json'
			},
			hostname: 'localhost',
			method: 'GET',
			path,
			port: config.openport,
			protocol: 'http:'
		}

		const req = http.request(params, res => {
			cb(res)
		})
		req.end()
	}
}

module.exports = utils
