const assert = require('assert')
const logger = require('./../lib/logger')

const loggerSpec = {}

loggerSpec['async logger list should return list of files available'] = done => {
	logger.list(true, (err, files) => {
		assert.equal(err, false)
		assert.ok(files instanceof Array)
		assert.ok(files.length > 1)
		done()
	})
}

loggerSpec['logger truncate should not throw on bad input'] = done => {
	assert.doesNotThrow(() => {
		logger.truncate('invalid', err => {
			assert.ok(err)
			done();
		})
	}, TypeError)
}

module.exports = loggerSpec