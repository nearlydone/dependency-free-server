const assert = require('assert')
const utils = require('./test-utils')
const app = require('./../index')

const api = {}

api['int:: index.init does not throw an error'] = done => {
	assert.doesNotThrow(() => {
		app.init(err => {
			done()
		})
	}, TypeError)
}

api['int:: ping responds with 200'] = done => {
	utils.getRequest('/', {}, res => {
		assert.equal(res.statusCode, 200)
		done()
	})
}

api['int:: user with no token responds with 403'] = done => {
	utils.getRequest('/api/users', {}, res => {
		assert.equal(res.statusCode, 403)
		done()
	})
}

api['int:: user with no params responds with 400'] = done => {
	utils.getRequest('/api/users', {
		token: 'abcd'
	}, res => {
		assert.equal(res.statusCode, 400)
		done()
	})
}

api['int:: unknown path responds with 404'] = done => {
	utils.getRequest('/fake', {}, res => {
		assert.equal(res.statusCode, 404)
		done()
	})
}

module.exports = api