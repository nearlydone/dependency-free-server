process.env.NODE_ENV = 'testing'

const loggerSpec = require('./logger.spec')
const utilSpec = require('./util.spec')
const apiSpec = require('./api.spec')

const runner = {
	count: {
		tests: () => Object.keys(runner.tests).length
	},
	report: (limit, success, errors) => {
		console.log(`
--------------- TEST REPORT --------------
Total tests: ${limit}
PASSED: ${success}
ERRORS: ${errors.length}`)

		if (errors.length) {
			console.log()
			console.log('----------- ERROR DETAILS -------------')
			errors.forEach(error => {
				console.log('\x1b[31m%s\x1b[0m', `${error.name}`)
				console.log(error.error)
			})
		}
		process.exit(0)
	},
	init: () => {
		const errors = []
		const limit = runner.count.tests()
		let success = 0
		let counter = 0

		Object.keys(runner.tests).forEach(test => {
			(() => {
				try {
					runner.tests[test](() => {
						console.log('\x1b[32m%s\x1b[0m', `${test}`)
						success++
						counter++
						if (counter === limit) {
							runner.report(limit, success, errors)
						}
					})
				} catch (e) {
					errors.push({
						name: test,
						error: e
					})
					counter++
					if (counter === limit) {
						runner.report(limit, success, errors)
					}
				}
			})()
		})
	},
	setTest: suite => {
		Object.keys(suite).forEach(spec => {
			if (runner.tests[spec]) {
				console.error(`${spec} -- is already defined`)
			} else {
				runner.tests[spec] = suite[spec]
			}
		})
	},
	tests: {}

}

runner.setTest(loggerSpec)
runner.setTest(utilSpec)
runner.setTest(apiSpec)

runner.init()
