const assert = require('assert')
const utils = require('./../lib/utils')

const utilSpec = {}

utilSpec['getANumber returns a number'] = done => {
	const num = utils.getANumber()
	assert.equal(typeof num, 'number')
	done()
}

utilSpec['getANumber returns 42'] = done => {
	const num = utils.getANumber()
	assert.equal(num, 42)
	done()
}

module.exports = utilSpec