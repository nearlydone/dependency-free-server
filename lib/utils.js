const crypto = require('crypto')
const fs = require('fs')
const https = require('https')
const path = require('path')
const querystring = require('querystring')
const strings = require('./translate')
const config = require('../config')

const utils = {
	getTemplate: (template, cb) => {
		if (!template) {
			return cb('Invalid template name')
		}
		const dirname = path.join(__dirname, '/../templates/')
		fs.readFile(`${dirname}${template}.html`, 'utf-8', (err, str) => {
			if (err) {
				return cb('Template not found')
			}
			fs.readFile(`${dirname}_header.html`, 'utf-8', (err, hstr) => {
				if (err) {
					return cb('Header not found')
				}
				fs.readFile(`${dirname}_footer.html`, 'utf-8', (err, fstr) => {
					if (err) {
						return cb('Footer not found')
					}
					return cb(null, utils.replaceStrings(`${hstr}${str}${fstr}`, template))
				})
			})
		})
	},
	getANumber: () => 42,
	getStaticAsset: (filename, cb) => {
		if (!filename.length) {
			cb('Invalid filename provided')
		}
		const public = path.join(__dirname, '/../dist/')
		fs.readFile(`${public}${filename}`, (err, data) => {
			if (err) {
				return cb('No file found')
			}
			return cb(false, data)
		})
	},
	hash: str => {
		if (typeof(str) == 'string' && str.trim().length > 0) {
			// return str
			const hash = crypto.createHmac('sha256', config.hashSecret).update(str).digest('hex')
		} else {
			return false
		}
	},
	randomString: len => {
		if (len > 0) {
			const source = 'qwertyuioplkjhgfdsazxcvbnm123457890';
			let random = []

			for (let i = 0, j = source.length; i < len; i++) {
				random.push(source[Math.floor(Math.random() * j)])
			}
			return random.join('')
		} else {
			return false
		}
	},
	replaceStrings: (html, template) => {
		if (html.match(/{([^}]*)}/)) {
			html = html.replace(/{([^}]*)}/, (whole, match) => {
				const [ group, item ] = match.split('.')
				if (strings[group] && strings[group][template] && strings[group][template][item]) {
					return strings[group][template][item]
				} else if (strings[group] && strings[group][item]) {
					return strings[group][item]
				} else {
					console.log(`group ${group} or item ${item} not defined for ${template}`)
				}
			})
			if (html.match(/{([^}]*)}/)) {
				html = utils.replaceStrings(html, template)
			}
		}
		return html
	},
	sendMessage: (phoneNumber, message, cb) => {
		if (phoneNumber.length === 11 && message.trim().length > 0 && message.trim().length < 160) {
			const payload = {
				From: config.twilio.senderNumber,
				To: phoneNumber,
				Body: message
			}
			const request = {
				protocol: 'https:',
				hostname: 'api.twilio.com',
				method: 'post',
				path: `/2010-04-01/accounts/${config.twilio.accountID}/messages.json`,
				auth: `${config.twilio.accountID}:${config.twilio.authToken}`,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': Buffer.byteLength(querystring.stringify(payload))
				}
			}
			const req = https.request(request, res => {
				cb(res.statusCode)
			})
			req.on('error', cb)
			req.write(querystring.stringify(payload))
			req.end()
		} else {
			cb(400, {
				message: 'Missing or invalid mandatory fields'
			})
		}
	}
}

module.exports = utils