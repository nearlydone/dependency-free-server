const os = require('os')
const v8 = require('v8')
const childProcess = require('child_process')

const data = require('./data')
const logger = require('./logger')

const handlers = {
	blankLine: lines => {
		lines = lines > 0 ? lines : 1
		for (let i = 0; i < lines; i++) {
			console.log('')
		}
	},
	center: str => {
		str = str.trim()
		const width = process.stdout.columns
		let padding = Math.floor((width - str.length) / 2)
		let line = ''
		while (padding > 0) {
			line += ' '
			padding--
		}
		console.log(`${line}${str}`)
	},
	exit: () => {
		process.exit(0)
	},
	info: {
		check: str => {
			const checkID = str.split('--')[1]
			if (checkID && checkID.length > 0) {
				data.read('checks', checkID, (err, data) => {
					if (err) {
						return console.log('There was an error reading check')
					}
					data = JSON.parse(data)
					handlers.blankLine()
					console.dir(data, {
						colors: true
					})
					handlers.blankLine()
				})
			}
		},
		log: str => {
			const filename = str.split('--')[1]
			if (filename && filename.length > 0) {
				logger.decompress(filename, (err, data) => {
					if (err) {
						console.error('Error reading archive')
					}
					const lines = data.split('\n')
					lines.forEach(line => {
						const log = JSON.parse(line)
						if (log.keys().length > 0) {
							console.dir(log, {
								colors: true
							})
							handlers.blankLine()
						}
					})
				})
			}
		},
		user: str => {
			const userID = str.split('--')[1]
			if (userID && userID.length > 0) {
				data.read('users', userID, (err, data) => {
					if (err) {
						return console.log('There was an error reading user')
					}
					data = JSON.parse(data)
					delete data.hashPassword
					handlers.blankLine()
					console.dir(data, {
						colors: true
					})
					handlers.blankLine()
				})
			}
		}
	},
	help: () => {
		const commands = new Map()
		commands.set('exit', 'Kill the CLI and the server')
		commands.set('man', 'Show this page')
		commands.set('help', 'Alias of "man"')
		commands.set('stats', 'Get performance statistics')
		commands.set('list users', 'Show a list of all undeleted users')
		commands.set('list checks --up --down', 'Show a list of active checks, optionally filtered')
		commands.set('list logs', 'Show a list of all logs available')
		commands.set('info user --{userID}', 'Show details of a specific user')
		commands.set('info check --{checkID}', 'Show details of a specific check')
		commands.set('info log --{filename}', 'Show details of a specific log file')

		handlers.hr()
		handlers.center('CLI MANUAL')
		handlers.hr()
		handlers.blankLine(2)

		commands.forEach((value, key) => {
			let title = '\x1b[35m' + key + '\x1b[0m'
			for (let i = 0, j = 40 - title.length; i < j; i++) {
				title += ' '
			}
			console.log(`${title}${value}`)
			handlers.blankLine()
		})
		handlers.blankLine(1)
		handlers.hr()
	},
	hr: () => {
		let width = process.stdout.columns
		let line = ''
		while (width > 0) {
			line += '-'
			width--
		}
		console.log(line)
	},
	list: {
		checks: str => {
			data.list('checks', (err, checks) => {
				if (err) {
					return console.log('Error readng list of checks')
				}
				handlers.blankLine()
				const flag = str.split('--')[1]
				if (flag === 'up' || flag === 'down') {
					checks.forEach(id => {
						data.read('checks', id, (err, check) => {
							if (err) {
								console.log('Error retrieving check')
							}
							check = JSON.parse(check)
							let line = ''
							if (check.state === flag || check.state === undefined) {
								line += 'ID: ' + check.id
								line += ' - ' + check.protocol + '://' + check.url
								line += ' - State: ' + check.state
								console.log(line)								
							}
						})						
					})
				} else {
					console.dir(checks.sort())
				}
			})
		},
		logs: () => {
			const ls = childProcess.spawn('ls', ['./.logs/'])
			ls.stdout.on('data', data => {
				const files = data.toString().split('\n')
				handlers.blankLine()
				files.forEach(log => {
					if (typeof (log) === 'string' && log.length > 0 && log.indexOf('-') > -1) {
						console.log(log.trim().split('.')[0])
					}
				})
				handlers.blankLine()
			})
		},
		users: () => {
			data.list('users', (err, ids) => {
				if (err) {
					return console.log('Error reading users')
				}
				handlers.blankLine()
				ids.forEach(id => {
					data.read('users', id, (err, user) => {
						if (err) {
							return console.log('Error reading user', id)
						}
						user = JSON.parse(user)
						let line = 'Name: ' + user.firstName + ' ' + user.givenName
						line += ' Phone: ' + user.phoneNumber
						line += ' Checks: ' + user.checks.length
						console.log(line)
					})
				})
			})
		}
	},
	stats: () => {
		const stats = new Map()
		stats.set('Load Average', os.loadavg().join(' '))
		stats.set('CPU Count', os.cpus().length)
		stats.set('Free memory', os.freemem())
		stats.set('Current allocated memory', v8.getHeapStatistics().malloced_memory)
		stats.set('Peak allocated memory', v8.getHeapStatistics().peak_malloced_memory)
		stats.set('Allocated heap use (%)', Math.round(v8.getHeapStatistics().used_heap_size / v8.getHeapStatistics().total_heap_size) * 100)
		stats.set('Available heap allocated (%)', Math.round(v8.getHeapStatistics().total_heap_size / v8.getHeapStatistics().heap_size_limit) * 100)
		stats.set('Uptime', os.uptime())

		handlers.hr()
		handlers.center('SYSTEM STATISTICS')
		handlers.hr()
		handlers.blankLine(2)
		stats.forEach((value, key) => {
			let title = '\x1b[33m' + key + '\x1b[0m'
			for (let i = 0, j = 40 - title.length; i < j; i++) {
				title += ' '
			}
			console.log(`${title}${value}`)
			handlers.blankLine()
		})
		handlers.blankLine(1)
		handlers.hr()
	}
}

module.exports = handlers