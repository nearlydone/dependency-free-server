const url = require('url')
const dns = require('dns')
const { performance, PerformanceObserver } = require('perf_hooks')
const util = require('util')
const debug = util.debuglog('perfmatters')

const datalib = require('./data')
const utils = require('./utils')
const config = require('../config')

const methods = ['delete', 'get', 'post', 'put']

const frontend = {
	account: {
		create: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('accountCreate', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		},
		deleted: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('accountDeleted', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		},
		edit: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('accountEdit', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		}
	},
	checks: {
		create: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('checksCreate', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		},
		list: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('checksList', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		}
	},
	favicon: (data, cb) => {
		if (data.method === 'get') {
			utils.getStaticAsset('favicon.ico', (err, file) => {
				if (err) {
					return cb(500, null, 'favicon')
				}
				return cb(200, file, 'favicon')
			})
		} else {
			return cb(405, null, 'favicon')
		}
	},
	index: (data, cb) => {
		if (data.method === 'get') {
			utils.getTemplate('index', (err, str) => {
				if (err) {
					return cb(500, null, 'html')
				}
				return cb(200, str, 'html')
			})
		} else {
			return cb(405, null, 'html')
		}
	},
	public: (data, cb) => {
		if (data.method === 'get') {
			const filename = data.path.replace('public/', '').trim()
			if (!filename.length) {
				return cb(404)
			}
			utils.getStaticAsset(filename, (err, str) => {
				if (err) {
					return cb(404, null, 'html')
				}
				let contentType = 'plain'
				if (filename.indexOf('.css') > -1) {
					contentType = 'css'
				}
				if (filename.indexOf('.js') > -1) {
					contentType = 'js'
				}
				if (filename.indexOf('.png') > -1) {
					contentType = 'png'
				}
				if (filename.indexOf('.jpg') > -1) {
					contentType = 'jpg'
				}
				if (filename.indexOf('.ico') > -1) {
					contentType = 'ico'
				}
				return cb(200, str, contentType)
			})
		} else {
			return cb(405, null, 'html')
		}
	},
	session: {
		create: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('sessionCreate', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		},
		delete: (data, cb) => {
			if (data.method === 'get') {
				utils.getTemplate('sessionDeleted', (err, str) => {
					if (err) {
						return cb(500, null, 'html')
					}
					return cb(200, str, 'html')
				})
			} else {
				return cb(405, null, 'html')
			}
		}
	}
}

const internal = {
	_check: {
		delete: (data, cb) => {
			if (data.querys.id.trim().length === 20) {
				datalib.read('checks', data.querys.id.trim(), err => {
					if (err) {
						return cb(404)
					}
					datalib.delete('checks', data.querys.id.trim(), err => {
						if (err) {
							cb(400, { 
								message: 'Error deleting token'
							})
						}
					})
				})
			} else {
				cb(400, {
					message: 'Missing required field'
				})
			}
		},
		get: (data, cb) => {
			const id = data.querys.id
			if (id) {
				datalib.read('checks', id, (err, check) => {
					if (err) {
						return cb(404)
					}
					if (!data.headers.token) {
						cb(403, {
							message: 'Missing token'
						})
					}
					check = JSON.parse(check)
					if (check.phoneNumber.trim().length === 11) {
						internal._tokens.verify(data.headers.token, check.phoneNumber.trim(), valid => {
							if (valid) {
								cb(200, check)
							} else {
								cb(403)
							}
						})
					} else {
						cb(400, {
							message: 'Missing required field'
						})
					}

				})
			} else {
				cb(400, {
					message: 'Missing required field'
				})
			}
		},
		post: (data, cb) => {
			data.payload = JSON.parse(data.payload)
			data.payload.timeoutSeconds = Number(data.payload.timeoutSeconds)
			const protocol = ['http', 'https'].indexOf(data.payload.protocol) > -1 ? data.payload.protocol : false
			const _url = data.payload.url.trim().length > 0 ? data.payload.url.trim() : false
			const method = methods.indexOf(data.payload.method.toLowerCase()) > -1 ? data.payload.method.toLowerCase() : false
			const successCodes = data.payload.successCodes.length > 0 ? data.payload.successCodes : false
			const timeout = data.payload.timeoutSeconds > 0 && data.payload.timeoutSeconds < 6 ? data.payload.timeoutSeconds : false
			const token = data.headers.token || false

			if (protocol && _url && method && successCodes && timeout && token) {
				datalib.read('tokens', token, (err, tdata) => {
					tdata = typeof tdata === 'object' ? tdata : JSON.parse(tdata || {})
					if (err) {
						return cb(404)
					}
					datalib.read('users', tdata.phoneNumber, (err, userdata) => {
						if (err) {
							return cb(400, {
								message: 'User not found'
							})
						}
						let udata = JSON.parse(userdata)
						if (typeof udata === 'string') {
							udata = JSON.parse(udata)
						}
						udata.checks = udata.checks || []
						if (udata.checks.length >= config.maxChecks) {
							return cb(400, {
								message: 'Max checks exceeded'
							})
						}

						const hostname = url.parse(`${protocol}://${_url}`, true).hostname
						dns.resolve(hostname, (err, records) => {
							if (err) {
								return cb(400, {
									message: 'DNS entries not found for hostname: ' + hostname
								})
							}

							const id = utils.randomString(20)
							const check = {
								id,
								phoneNumber: tdata.phoneNumber,
								protocol,
								_url,
								method,
								timeout,
								successCodes
							}
							datalib.create('checks', id, check, err => {
								if (err) {
									return cb(500, {
										message: 'Could not create check file'
									})
								}
								udata.checks.push(id)
								datalib.update('users', tdata.phoneNumber, typeof udata === 'string' ? udata : JSON.stringify(udata), err => {
									if (err) {
										cb(500, {
											message: 'Could not update user'
										})
									}
									cb(200, check, 'json')
								})
							})
						})
					})
				})
			} else {
				cb(400, {
					message: 'Missing required inputs'
				})
			}
		},
		put: (data, cb) => {
			const id = data.payload.id ? data.payload.id : false
			const protocol = ['http', 'https'].indexOf(data.payload.protocol) > -1 ? data.payload.protocol : false
			const url = data.payload.url.trim().length > 0 ? data.payload.url.trim() : false
			const method = methods.indexOf(data.payload.method.toLowerCase()) > -1 ? data.payload.method.toLowerCase() : false
			const success = data.payload.successCodes.length > 0 ? data.payload.successCodes : false
			const timeout = data.payload.timeoutSeconds > 0 && data.payload.timeoutSeconds < 5 ? data.payload.timeoutSeconds : false
			const token = data.headers.token || false
			if (!id) {
				return cb(400, {
					message: 'Missing required field'
				})
			}
			if (protocol || url || method || success || timeout) {
				datalib.read('checks', id, (err, cdata) => {
					if (err) {
						return cb(400, {
							message: 'Check id did not exist'
						})
					}
					cdata.protocol = protocol || ''
					cdata.url = url || ''
					cdata.method = method || ''
					cdata.success = success || []
					cdata.timeout = timeout || 5
					datalib.update('checks', id, cdata, err => {
						if (err) {
							return cb(400, {
								message: 'Check update failed'
							})
						}
						cb(200)
					})
				})
			} else {
				cb(400, {
					message: 'Nothing to update'
				})
			}
		}
	},
	_tokens: {
		delete: (data, cb) => {
			if (data.querys.id.trim().length === 20) {
				datalib.read('tokens', data.querys.id.trim(), (err, sdata) => {
					if (err) {
						return cb(404)
					}
					datalib.delete('tokens', data.querys.id.trim(), err => {
						if (err) {
							cb(400, { 
								message: 'Error deleting token'
							})
						}
					})
				})
			} else {
				cb(400, {
					message: 'Missing required field'
				})
			}
		},
		get: (data, cb) => {
			if (data.querys.id.trim().length === 20) {
				datalib.read('tokens', data.querys.id.trim(), (err, tdata) => {
					if (err) {
						return cb(404)
					}
					cb(200, tdata)
				})
			} else {
				cb(400, {
					message: 'Missing required field'
				})
			}

		},
		post: (data, cb) => {
			performance.mark('token post - entered')
			data.payload = JSON.parse(data.payload)
			const phoneNumber = data.payload.phoneNumber.trim().length === 11 ? data.payload.phoneNumber.trim() : false
			const password = data.payload.password.trim().length > 0 ? data.payload.password.trim() : false
			performance.mark('token post - data set')
			if (phoneNumber && password) {
				if (phoneNumber.length === 11) {
					performance.mark('token post - user lookup start')
					datalib.read('users', phoneNumber, (err, sdata) => {
						performance.mark('token post - user lookup complete')
						if (err) {
							return cb(404)
						}
						performance.mark('token post - password hashing start')
						const passwordHash = utils.hash(password)
						performance.mark('token post - password hashing done')
						if (passwordHash === sdata.password) {
							performance.mark('token post - create token data')
							let tokenID = utils.randomString(20)
							const token = {
								phoneNumber,
								id: tokenID,
								expires: Date.now() + 1000 * 60 * 60
							}
							performance.mark('token post - created token data')
							performance.mark('token post - token write start')
							datalib.create('tokens', tokenID, token, err => {
								performance.mark('token post - token write complete')
								if (err) {
									return cb(500, {
										message: 'Could not create new token'
									})
								}
								performance.measure('token post whole', 'token post - entered', 'token post - token write complete')
								performance.measure('token post user input', 'token post - entered', 'token post - data set')
								performance.measure('token post user lookup', 'token post - user lookup start', 'token post - user lookup complete')
								performance.measure('token post store token data', 'token post - token write start', 'token post - token write complete')

								const perf = new PerformanceObserver((list, obs) => {
									list.getEntries().forEach(entry => {
										debug('\x1b[33m%s\x1b[0m', `${entry.name} :: ${entry.duration}`)
									})
									performance.clearMarks()
  									obs.disconnect()
								})
								perf.observe({ 
									entryTypes: ['measure'], 
									buffered: true 
								})
								cb(200, typeof token === 'string' ? token : JSON.stringify(token || {}))
							})
						} else {
							cb(400, {
								message: 'Password mismatch'
							})
						}
					})
				} else {
					cb(400, {
						message: 'Missing required field'
					})
				}
			} else {
				cb(400, {
					message: 'Missing mandatory field'
				})
			}
		},
		put: (data, cb) => {
			if (data.payload.id.trim().length === 20 && data.payload.extend == true) {
				datalib.read('tokens', data.payload.id, (err, tdata) => {
					if (err) {
						return cb(400, {
							message: 'Token does not exist'
						})
					}
					if (tdata.expires > Date.now()) {
						tdata.expires = Date.now() + 1000 * 60 * 60
						datalib.update('tokens', id, tdata, err => {
							if (err) {
								return cb(500, {
									message: 'Could not update token'
								})
							}
							cb(200)
						})
					} else {

					}
				})
			} else {
				cb(400, {
					message: 'Missing mandatory field'
				})
			}
		},
		verify: (id, phoneNumber, cb) => {
			datalib.read('tokens', id, (err, tdata) => {
				if (err) {
					return cb(false)
				}
				tdata = JSON.parse(tdata)
				if (tdata.phoneNumber === phoneNumber && tdata.expires > Date.now()) {
					cb(true)
				} else {
					cb(false)
				}
			})
		}
	},
	_users: {
		delete: (data, cb) => {
			data.payload = JSON.parse(data.payload)
			if (data.payload.phoneNumber.trim().length === 11) {
				datalib.read('users', data.payload.phoneNumber.trim(), (err, sdata) => {
					if (err) {
						return cb(404)
					}
					datalib.delete('users', data.payload.phoneNumber.trim(), err => {
						if (err) {
							cb(400, { 
								message: 'Error deleting user'
							})
						}
					})
				})
			} else {
				cb(400, {
					message: 'Missing required field'
				})
			}
		},
		get: (data, cb) => {
			if (!data.headers.token) {
				return cb(403, {
					message: 'Missing token'
				})
			}
			if (data.querys.phoneNumber && data.querys.phoneNumber.trim().length === 11) {
				internal._tokens.verify(data.headers.token, data.querys.phoneNumber.trim(), valid => {
					if (valid) {
						datalib.read('users', data.querys.phoneNumber.trim(), (err, sdata) => {
							if (err) {
								return cb(403)
							}
							delete sdata.password
							cb(200, sdata, 'json')
						})
					} else {
						return cb(403, {
							message: 'Token not valid'
						})
					}
				})
			} else {
				return cb(400, {
					message: 'Missing required field'
				})
			}
		},
		post: (data, cb) => {
			data.payload = JSON.parse(data.payload)
			const firstName = data.payload.firstName.trim().length > 0 ? data.payload.firstName.trim() : false
			const givenName = data.payload.givenName.trim().length > 0 ? data.payload.givenName.trim() : false
			const phoneNumber = data.payload.phoneNumber.trim().length === 11 ? data.payload.phoneNumber.trim() : false
			const password = data.payload.password.trim().length > 0 ? data.payload.password.trim() : false
			const terms = data.payload.terms == true ? true : false
			if (firstName && givenName && phoneNumber && password && terms) {
				datalib.read('users', phoneNumber, err => {
					if (err) {
						const user = {
							firstName,
							givenName,
							phoneNumber,
							password: utils.hash(password),
							terms: true,
							checks: []
						}
						datalib.create('users', phoneNumber, typeof user === 'string' ? user : JSON.stringify(user || {}), err => {
							if (err) {
								return cb(500, {
									message: 'Could not create new user'
								})
							}
							cb(200)
						})
					} else {
						cb(400, {
							message: 'User already exists'
						})
					}
				})
			} else {
				cb(400, {
					message: 'Missing required fields'
				})
			}
		},
		put: (data, cb) => {
			if (data.payload.phoneNumber.trim().length === 11) {
				if (data.payload.firstName.trim() || data.payload.givenName.trim() || data.payload.password.trim()) {
					datalib.read('users', data.payload.phoneNumber, (err, rdata) => {
						if (err) {
							return cb(400, {
								message: 'User does not exist'
							})
						}
						if (data.payload.firstName.trim().length > 0) {
							rdata.firstName = data.payload.firstName.trim()
						}
						if (data.payload.givenName.trim().length > 0) {
							rdata.givenName = data.payload.givenName.trim()
						}
						if (data.payload.password.trim().length > 0) {
							rdata.password = utils.hash(data.payload.password.trim())
						}
						datalib.update('users', phoneNumber, rdata, err => {
							if (err) {
								return cb(500, {
									message: 'Could not update user'
								})
							}
							cb(200)
						})
					})
				} else {
					cb(400, {
						message: ' Nothing to update'
					})
				}
			} else {
				cb(400, {
					message: 'Missing mandatory field'
				})
			}
		}
	}
}

const handlers = {
	account: {
		create: frontend.account.create,
		deleted: frontend.account.deleted,
		edit: frontend.account.edit
	},
	check: (data, cb) => {
		if (methods.indexOf(data.method) === -1) {
			return cb(405)
		}
		internal._check[data.method](data, cb)
	},
	checks: {
		create: frontend.checks.create,
		list: frontend.checks.list
	},
	favicon: frontend.favicon,
	healthCheck: (data, cb) => {
		cb(200)
	},
	index: frontend.index,
	public: frontend.public,
	session: {
		create: frontend.session.create,
		delete: frontend.session.delete
	},
	tokens: (data, cb) => {
		if (methods.indexOf(data.method) === -1) {
			return cb(405)
		}
		internal._tokens[data.method](data, cb)
	},
	users: (data, cb) => {
		if (methods.indexOf(data.method) === -1) {
			return cb(405)
		}
		internal._users[data.method](data, cb)
	},
	notFound: (data, cb) => {
		cb(404)
	}
}

module.exports = handlers