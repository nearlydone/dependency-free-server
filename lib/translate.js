const page = {
	accountCreate: {
		pageTitle: 'Create account',
		description: 'Sign up for a new account',
		bodyClass: 'accountCreate'
	},
	accountDeleted: {
		pageTitle: 'Account successfully deleted',
		description: 'Your account has been removed from the system',
		bodyClass: 'accountDeleted'
	},
	accountEdit: {
		pageTitle: 'Edit account',
		description: 'Amend your account details',
		bodyClass: 'accountEdit'
	},
	checksCreate: {
		pageTitle: 'Create a new check',
		description: 'Create a new monitor for a website',
		bodyClass: 'checksCreate'
	},
	checksList: {
		pageTitle: 'Dashboard',
		description: 'All of your website monitors',
		bodyClass: 'checksList'
	},
	index: {
		pageTitle: 'Homepage',
		description: 'Home page of dep free server',
		bodyClass: 'home'
	},
	sessionCreate: {
		pageTitle: 'Log in to your account',
		description: 'Enter your phone number and password to log in',
		bodyClass: 'sessionCreate'
	},
	sessionDeleted: {
		pageTitle: 'Log have been logged out of your account',
		description: 'Log in again if you wish',
		bodyClass: 'sessionDelete'
	}
}

const site = {
	application: 'Dependency free server',
	baseUrl: '/',
	companyName: 'Nearly Done Ltd',
	currentYear: '2018',
}

module.exports = {
	page,
	site
}