const fs = require('fs')
const path = require('path')

const basepath = path.join(__dirname, '../.data/')

const lib = {
	create: (directory, filename, data, cb) => {
		fs.open(`${basepath}${directory}/${filename}.json`, 'wx', (err, desc) => {
			if (err) {
				return cb(err)
			}
			fs.writeFile(desc, typeof data === 'string' ? data : JSON.stringify(data), err => {
				if (err) {
					return cb(err)
				}
				fs.close(desc, err => {
					if (err) {
						return cb(err)
					}
					cb(false)
				})
			})
		})
	},
	delete: (directory, filename, cb) => {
		fs.unlink(`${basepath}${directory}/${filename}.json`, cb)
	},
	list: (directory, cb) => {
		fs.readdir(`${basepath}${directory}/`, (err, data) => {
			if (err) {
				return cb(err)
			}
			let files = data.map(file => file.replace('.json', ''))
			if (files.indexOf('.gitkeep') > -1) {
				files.splice(files.indexOf('.gitkeep'), 1)
			}
			cb(err, files)
		})
	},
	read: (directory, filename, cb) => {
		fs.readFile(`${basepath}${directory}/${filename}.json`, 'utf-8', (err, data) => {
			cb(err, data)
		})
	},
	update: (directory, filename, data, cb) => {
		fs.open(`${basepath}${directory}/${filename}.json`, 'r+', (err, desc) => {
			if (err) {
				return cb(err)
			}
			fs.ftruncate(desc, 0, err => {
				if (err) {
					return cb(err)
				}
				fs.writeFile(`${basepath}${directory}/${filename}.json`, data, err => {
					if (err) {
						return cb(err)
					}
					fs.close(desc, err => {
						if (err) {
							return cb(err)
						}
						cb(false)
					})
				})
			})
		})
	}
}

module.exports = lib