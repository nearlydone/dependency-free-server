const fs = require('fs')
const path = require('path')
const zlib = require('zlib')
const basepath = path.join(__dirname, '../.logs/')

const logger = {
	append: (logID, data, cb) => {
		fs.open(`${basepath}${logID}.log`, `a`, (err, fileDescriptor) => {
			if (err) {
				console.log('fs.open error')
				return cb('Could not open log file')
			}
			fs.appendFile(fileDescriptor, `${data}\n`, err => {
				if (err) {
					return cb('Error, appending to log file')
				}
				fs.close(fileDescriptor, err => {
					if (err) {
						return cb('Error closing file')
					}
				})
			})
		})
	},
	compress: (logID, archiveID, cb) => {
		const source = `${logID}.log`
		const dest = `${archiveID}.gz.b64`
		fs.readFile(`${basepath}${source}`, 'utf-8', (err, data) => {
			if (err) {
				return cb(err)
			}
			zlib.gzip(data, (err, buffer) => {
				if (err) {
					return cb(err)
				}
				fs.open(`${basepath}${dest}`, `wx`, (err, fileDescriptor) => {
					if (err) {
						return cb(err)
					}
					fs.writeFile(fileDescriptor, buffer.toString('base64'), err => {
						if (err) {
							cb(err)
						}
						fs.close(fileDescriptor, err => {
							if (err) {
								return cb(err)
							}
							cb(null)
						})
					})
				})
			})
		})
	},
	decompress: (archiveID, cb) => {
		const source = `${archiveID}.gz.b64`
		const dest = `${archiveID}.log`
		fs.readFile(`${basepath}${source}`, 'utf-8', (err, zip) => {
			if (err) {
				return cb(err)
			}
			let buffer = Buffer.from(zip, 'base64')
			zlib.gunzip(buffer, (err, data) => {
				if (err) {
					return cb(err)
				}
				cb(null, data.toString())
			})
		})
	},
	init: () => {

	},
	list: (compressedWanted, cb) => {
		fs.readdir(basepath, (err, data) => {
			if (err) {
				return cb(err, data)
			}
			if (!data || !data.length) {
				return cb(null, [])
			}
			let files = []
			data.forEach(filename => {
				if (filename.indexOf('.log') > -1) {
					files.push(filename.replace('.log', ''))
				}
				if (filename.indexOf('.gz.b64') > -1 && compressedWanted) {
					files.push(filename.replace('.gz.b64', ''))
				}
			})
			cb(false, files)
		})
	},
	truncate: (logID, cb) => {
		fs.truncate(`${basepath}${logID}.log`, 0, err => {
			if (err) {
				return cb(err)
			}
			cb(false)
		})
	}
}

module.exports = logger