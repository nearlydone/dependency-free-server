const fs = require('fs')

let environments = {
	testing: {
		cert: fs.readFileSync('./certs/cert.pem'),
		envName: 'testing',
		hashSecret: 'supersecret',
		key: fs.readFileSync('./certs/key.pem'),
		maxChecks: 5,
		openport: 5001,
		secureport: 5002,
		twilio: {
			accountID: 'qwert12345',
			authToken: 'asdfg45678',
			senderNumber: '01234567890'
		}
	},
	staging: {
		cert: fs.readFileSync('./certs/cert.pem'),
		envName: 'staging',
		hashSecret: 'supersecret',
		key: fs.readFileSync('./certs/key.pem'),
		maxChecks: 5,
		openport: 4001,
		secureport: 4002,
		twilio: {
			accountID: 'qwert12345',
			authToken: 'asdfg45678',
			senderNumber: '01234567890'
		}
	},
	production: {
		cert: fs.readFileSync('./certs/cert.pem'),
		envName: 'production',
		hashSecret: 'supersecretdonottellanyone',
		key: fs.readFileSync('./certs/key.pem'),
		maxChecks: 5,
		openport: 4101,
		secureport: 4102,
		twilio: {
			accountID: 'qwert12345',
			authToken: 'asdfg45678',
			senderNumber: '01234567890'
		}
	}
}

module.exports = process.env.NODE_ENV ? environments[process.env.NODE_ENV.toLowerCase()] || environments.staging : environments.staging

