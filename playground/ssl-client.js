const tls = require('tls')
const fs = require('fs')

const sslConfig = {
	ca: fs.readFileSync('../certs/cert.pem')
}

const broadcast = 'ping'

const client = tls.connect(6002, sslConfig, () => {
	client.write(broadcast)
})

client.on('data', message => {
	console.log(`Client sent ${broadcast} and got back ${message.toString()} from server`)
})