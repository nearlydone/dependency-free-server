const tls = require('tls')
const fs = require('fs')

const sslConfig = {
	cert: fs.readFileSync('../certs/cert.pem'),
	key: fs.readFileSync('../certs/key.pem')
}

const server = tls.createServer(sslConfig, connection => {
	const broadcast = 'pong'
	connection.write(broadcast)

	connection.on('data', message => {
		console.log(`Server sent ${broadcast} and got back ${message.toString()} from client`)
	})
})

server.listen(6002)