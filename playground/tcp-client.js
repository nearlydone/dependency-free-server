const tcp = require('net')

const broadcast = 'ping'

const client = tcp.createConnection({ port: 6001 }, () => {
	client.write(broadcast)
})

client.on('data', message => {
	console.log(`Client sent ${broadcast} and got back ${message.toString()} from server`)
})