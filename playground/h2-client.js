const http2 = require('http2')

const client = http2.connect('http://localhost:6001')

const req = client.request({
	':path': '/'
})

let payload = ''

req.on('data', data => {
	payload += data
})

req.on('end', () => {
	console.log(payload)
})

req.end()