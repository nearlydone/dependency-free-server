const tcp = require('net')

const server = tcp.createServer(connection => {
	const broadcast = 'pong'
	connection.write(broadcast)

	connection.on('data', message => {
		console.log(`Server sent ${broadcast} and got back ${message.toString()} from client`)
	})
})

server.listen(6001)