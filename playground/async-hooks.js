const async = require('async_hooks')
const fs = require('fs')

const context = false

const hooks = {
	init: (asyncID, type, triggerID, resource) => {
		fs.writeSync(1, `init hook ${asyncID}\n`)
	},
	before: asyncID => {
		fs.writeSync(1, `before hook ${asyncID}\n`)
	},
	after: asyncID => {
		fs.writeSync(1, `after hook ${asyncID}\n`)
	},
	destroy: asyncID => {
		fs.writeSync(1, `destroy hook ${asyncID}\n`)
	},
	promiseResolve: asyncID => {
		fs.writeSync(1, `promiseResolve hook ${asyncID}\n`)
	}
}

const hook = async.createHook(hooks)
hook.enable()

const sayTime = cb => {
	setInterval(() => {
		fs.writeSync(1, `In interval context is ${async.executionAsyncId()} \n`)
		cb(Date.now())
	}, 1000)
}

sayTime(time => {
	fs.writeSync(1, `The time is ${time}\n`)
})
