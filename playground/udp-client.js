const dgram = require('dgram')

const client = dgram.createSocket('udp4')

const message = 'Example message to send to server'

const buffer = Buffer.from(message)

client.send(buffer, 6001, 'localhost', err => {
	client.close()
})
