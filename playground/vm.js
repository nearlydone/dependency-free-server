const vm = require('vm')

const context = {
	something: 42
}

const script = new vm.Script(`
	something = something * something
	const another = something / 2
	const third = something * another
`)

script.runInNewContext(context)
console.log(context)