const repl = require('repl')

repl.start({
	prompt: ':ND:>',
	eval: str => {
		console.log(`Evaluated ${str}`)
		if (str.indexOf('fizz') > -1) {
			console.log('buzz')
		}
	}
})