const fs = require('fs')
const http = require('http')
const https = require('https')
const urllib = require('url')
const debug = require('util').debuglog('workers')
const datalib = require('./lib/data')
const logger = require('./lib/logger')
const utils = require('./lib/utils')

const workers = {
	alertUser: check => {
		const message = `ALERT: Check for ${check.method.toUpperCase()} ${check.protocol}://${check.url} is currently ${check.state}`
		utils.sendMessage(check.phoneNumber, message, err => {
			if (err) {
				debug('Error: user state change failed to be sent to user')
			} else {
				debug('Success: user alerted to status change')
			}
		})
	},
	check: check => {
		let outcome = {
			error: false,
			response: false
		}
		let outcomeSent = false;
		const url = urllib.parse(`${check.protocol}://${check.url}`, true)
		const hostname = url.hostname
		const path = url.path
		const request = {
			protocol: `${check.protocol}:`,
			hostname,
			method: check.method.toUpperCase(),
			path,
			timeout: check.timeout * 1000
		}
		const requester = check.protocol === 'https' ? https : http
		const req = requester.request(request, res => {
			outcomeSent = false
			outcome.response = res.statusCode
			if (!outcomeSent) {
				workers.processOutcome(check, outcome)
				outcomeSent = true
			}
		})
		req.on('error', err => {
			outcome.error = {
				error: true,
				message: err
			}
			if (!outcomeSent) {
				workers.processOutcome(check, outcome)
				outcomeSent = true
			}
		})
		req.on('timeout', err => {
			outcome.error = {
				error: true,
				message: 'timeout'
			}
			if (!outcomeSent) {
				workers.processOutcome(check, outcome)
				outcomeSent = true
			}
		})
		req.end()
	},
	checks: () => {
		datalib.list('checks', (err, data) => {
			if (err) {
				debug('Error: No checks found', err)
			} else {
				data.forEach(check => {
					datalib.read('checks', check, (err, cdata) => {
						if (err) {
							debug('Error: Could not read check', check, err)
						} else {
							workers.validate(JSON.parse(cdata))
						}
					})
				})
			}
		})
	},
	dailyLogs: () => {
		setInterval(workers.rotateLogs, 1000 * 60 * 60 * 24)
	},
	init: () => {
		workers.checks()
		workers.loop()
		workers.rotateLogs()
		workers.dailyLogs()
	},
	log: (check, outcome, state, alerted, timestamp) => {
		const log = {
			check,
			outcome,
			state,
			alerted,
			timestamp
		}
		logger.append(check.id, JSON.stringify(log), err => {
			if (err) {
				debug('Logging to file errored')
			} else {
				debug('Logging to file successful')
			}
		})
	},
	loop: () => {
		setInterval(workers.checks, 60 * 1000)
	},
	processOutcome: (check, outcome) => {
		debug(outcome)
		if (outcome.error) {
			debug(outcome.error.error)
		} else {
			workers.log(check, outcome, check.state, false, Date.now())
			// TODO:: logic needs reworking - will do with unit tests
			if (outcome.response && check.successCodes.indexOf(outcome.response) > -1) {
				if (check.lastChecked && check.state !== (outcome.response && check.successCodes.indexOf(outcome.response) > -1)) {
					// warning
					check.lastChecked = new Date()
					datalib.update('checks', check.id, check, err => {
						if (err) {
							debug(err)
						} else {
							workers.alertUser(check)
						}
					})
				} else {
					// no warning
				}
			}
		}
	},
	rotateLogs: () => {
		logger.list(false, (err, logs) => {
			if (err || !logs.length) {
				debug('Error or no logs returned')
			} else {
				logs.forEach(log => {
					const logID = log.replace('.log', '')
					const date = new Date()
					const archiveID = `${logID}-${date.toISOString().split('T')[0]}`
					logger.compress(logID, archiveID, err => {
						if (err) {
							debug('Error compressng log file', logID)
						} else {
							logger.truncate(logID, err => {
								if (err) {
									debug('Error clearing log file')
								} else {
									debug('Log file cleared')
								}
							})
						}
					})
				})
			}
		})
	},
	validate: check => {
		check = typeof check === 'object' ? check : JSON.parse(check || {})
		check.id = check.id.trim().length === 20 ? check.id.trim() : false
		check.method = ['delete', 'get', 'post', 'put'].indexOf(check.method) > -1 ? check.method.trim() : false
		check.phoneNumber = check.phoneNumber.trim().length === 11 ? check.phoneNumber.trim() : false
		check.protocol = ['http', 'https'].indexOf(check.protocol) > -1 ? check.protocol.trim() : false
		check.successCodes = check.successCodes.length > 0 ? check.successCodes : false
		check.timeout = check.timeout > 0 && check.timeout < 6 ? check.timeout : false
		check.url = check.url.trim().length > 0 ? check.url.trim() : false

		check.state = ['down', 'up'].indexOf(check.state) > -1 ? check.state : 'down'
		check.lastChecked = check.lastChecked > 0 ? check.lastChecked : false

		if (check.id && check.method && check.phoneNumber && check.protocol && check.successCodes && check.timeout && check.url) {
			workers.check(check)
		} else {
			debug('Validate: malformed check object', check, check.id, check.method, check.phoneNumber, check.protocol, check.successCodes, check.timeout, check.url)
		}
	}
}

module.exports = workers