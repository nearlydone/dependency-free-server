# Dependancy free server

## Uptime checker

This repository demonstrates usage of only Node and it's native modules to create a functional non-trivial server. You'll notice that there is no `package.json` file and no need to `npm install` anything.

Simply `node index` will start up the server, watchers and CLI. 

If no environement variables are set then go to `http://localhost:5001` in a browser to access the application.
